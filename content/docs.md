# Docs

  - [docs.system-transparency.org][]: User and development documentation
  - [documentation/archive][]: Weekly meeting minutes and other project notes

[docs.system-transparency.org]: https://docs.system-transparency.org/
[documentation/archive]: https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/archive
