System Transparency is a free and open source software project that aims to
provide the components necessary to build, operate and verify transparent
systems. A system is transparent if there exists a globally discoverable and
verifiable link between its hardware, source code and runtime behavior.

Conceptually a transparent system requires the following:

- verified boot,
- measured boot,
- reproducible builds,
- transparency logging with global consistency, and
- remote attestation.

The specific technologies that the System Transparency project makes use of (or
is planning to use) are coreboot, UEFI Secure Boot, TCG TPM 2.0, AMD SEV-SNP,
Debian reproducible builds and Sigsum.
