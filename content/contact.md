# Contact

Chat with users and developers on IRC and Matrix.  The rooms are bridged so it
does not matter which one you choose.

  - IRC: #system-transparency @ OFTC.net
  - Matrix: [#system-transparency:matrix.org][]

Subscribe to the System Transparency [mailing lists][] by sending an email with
the subject 'subscribe' to

    st-announce-join (at) lists.system-transparency.org
    st-discuss-join (at) lists.system-transparency.org

or use the form at each list's info page.  Once subscribed, you can provide
feedback, report issues, ask questions, and such on the st-discuss list.  You
can also file issues and submit merge requests on [GitLab][].

Join open video/voice meetings on Mondays at 1200 UTC.

  - Jitsi: [meet.glasklar.is/ST-weekly][]
  - Notes: [pad.sigsum.org/ST-YYYYMMDD][]

[#system-transparency:matrix.org]: https://app.element.io/#/room/#system-transparency:matrix.org
[mailing lists]: https://lists.system-transparency.org/mailman3/postorius/lists/
[GitLab]: https://git.glasklar.is/system-transparency/
[meet.glasklar.is/ST-weekly]: https://meet.glasklar.is/ST-weekly
[pad.sigsum.org/ST-YYYYMMDD]: https://pad.sigsum.org/p/ST-YYYYMMDD
