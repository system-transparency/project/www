# About

System Transparency is a free and open-source software project that was
[announced publicly](https://mullvad.net/en/blog/system-transparency-future) by
Mullvad VPN in June, 2019.  The goal is to provide the necessary building blocks
for operating and verifying transparent systems.

## Maintenance

Maintenance and development of System Transparency is lead by [Glasklar Teknik
AB][].

[Glasklar Teknik AB]: https://www.glasklarteknik.se/

## Funding and sponsors

Glasklar Teknik AB's work on System Transparency is paid for by [Mullvad VPN
AB][].

[Mullvad VPN AB]: https://www.mullvad.net/
