# www.system-transparency.org 

[Hugo][] source to generate [www.system-transparency.org][].

**Status:** work in progress.

[Hugo]: https://gohugo.io/
[www.system-transparency.org]: https://www.system-transparency.org

## Getting started

  1. Run `git submodule update --init --recursive`
  2. Install Hugo
     - Debian: `apt install hugo`
     - Or [install from source][]
  3. Try serving the website locally
     1. Run `hugo serve`
     2. Browse http://localhost:1313

[install from source]: https://gohugo.io/installation/linux/#build-from-source

## Generate website for publishing

  1. Check that it runs as expected locally (see getting started, step 3).
  2. Run `hugo` to create the `public` repository that should be deployed.
